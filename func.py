#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math
import re
import os
import signal
import traceback
import time

def helpstub():
	return "Ja jebie, jak nie potrafisz helpa, to sie naucz"

def openstub():
	return "Nie ma otwierania!"

def evalstub():
	return "Co chcesz do evala jeszcze?"

# Here start builtin functions

def quit(args):
	args.commqueue.put("QUIT :Bai!")
	os.kill(os.getpid(), signal.SIGTERM)

def seen(args):
	if len(args.mesa) > 4:
		param = args.mesa[4]
	else:
		return
	args.scc.execute("SELECT * FROM seen WHERE kto LIKE ?", (param,))
	brb = args.scc.fetchone()
	if param == args.botclass.nick:
		args.commqueue.put("NOTICE " + args.mesa[0].split(":")[1].split("!")[0] +" :Heej, to ja!")
		return
	elif param in args.names:
		args.commqueue.put("NOTICE " + args.mesa[0].split(":")[1].split("!")[0] + " :Przecież " + param + " jest cały czas na kanale! Jak nie widzisz, to okulista się kłania :)")
	if brb:
		tro = brb[2]
		if brb[2] == "QUIT": tro = "at quit"
		elif brb[2] == "PART": tro = "at part"
		elif brb[2] == "JOIN": tro = "at join"
		elif brb[2].split(" ")[0] == "NICK": tro = "przy zmianie nicka na %s" % (brb[2].split(" ")[1])
		xyz = u"NOTICE %s :%s widziałem ostatnio %s: %s".encode('utf-8') % ( args.mesa[0].split("!")[0][1:], brb[0], brb[1], tro)
		args.commqueue.put(xyz)
		valki = (brb[0], args.mesa[0].split("!")[0][1:])
		args.scc.execute("SELECT * FROM szukal WHERE kogo=? AND kto=?", (valki))
		if(not(args.scc.fetchone())): args.scc.execute("INSERT INTO szukal VALUES (?, ?)", valki)
	else:
		args.commqueue.put("NOTICE %s :Sorry, ale nie znam żadnego %s" % (args.mesa[0].split("!")[0][1:], param))

def calc(args):
	request = ''
	if len(args.mesa) > 4:
		co = " ".join(args.mesa[4:])
		try:
			request = eval(co, {"degrees": math.degrees, "radians": math.radians, "tg": math.tan, "tan": math.tan, "sin": math.sin, "cos": math.cos, "abs": abs, "sqrt": math.sqrt, "pi": math.pi, "log": math.log, "floor": math.floor, "open": openstub, "help": helpstub, "eval": evalstub, "execfile": openstub, "input": openstub, "raw_input": openstub, "memoryview": openstub})
		except:
			traceback.print_exc()
			args.commqueue.put("PRIVMSG " + args.wyjscie + " :Coś nie halo z Twym zapytaniem.")
			return
		lr = 0
		if type(request) == unicode:
			requ = request
			lr = len(requ)
		else:
			requ = str(request)
			lr = len(str(requ))
		if lr > 240:
			args.commqueue.put("PRIVMSG " + args.wyjscie + " :Too long bejbe")
			return
		args.commqueue.put("PRIVMSG " + args.wyjscie + " :" + args.mesa[0].split("!")[0][1:] + ": " + requ.replace("\n", ";"))

def ogarnick(args):
	args.botclass.nickchange(args.botclass.orignick)
	args.commqueue.put("NICK " + args.botclass.nick)

def identify(args):
	args.commqueue.put("WHOIS :" + args.mesa[0].split("!")[0][1:])
	args.commqueue.put("PRIVMSG " + args.wyjscie + " :" + args.mesa[0].split("!")[0][1:] + ": Ok.")

def identified(args):
	if args.mesa[0].split("!")[0][1:] in args.botclass.authsyf.keys():
		if args.botclass.authsyf[mesa[0].split("!")[0][1:]] in args.botclass.admins:
			args.commqueue.put("PRIVMSG " + args.wyjscie + " :" + args.mesa[0].split("!")[0][1:] + ": Znam Cię, jesteś " + args.botclass.authsyf[args.mesa[0].split("!")[0][1:]] + "... Nawet masz prawa admina!")
		else:
			args.commqueue.put("PRIVMSG " + args.wyjscie + " :" + args.mesa[0].split("!")[0][1:] + ": Znam Cię, jesteś " + args.botclass.authsyf[mesa[0].split("!")[0][1:]])
	else:
		commqueue.put("PRIVMSG " + args.wyjscie + " :" + args.mesa[0].split("!")[0][1:] + ": Nie znam Cię, albo nie ma Cię w nickservie.")

def whois(args):
	if len(args.mesa) > 4:
		if args.mesa[4] in args.botclass.authsyf:
			args.commqueue.put("PRIVMSG " + args.wyjscie + " :" + args.mesa[0].split("!")[0][1:] + ": " + args.mesa[4] + " to " + args.botclass.authsyf[mesa[4]] )
		else:
			args.commqueue.put("PRIVMSG " + args.wyjscie + " :" + args.mesa[0].split("!")[0][1:] + ": Nie znam " + args.mesa[4])

#def resplag(args):
#	if len(args.mesa) > 4:
#		args.botclass.setresplag(int(args.mesa[4]))
#		args.commqueue.put("PRIVMSG " + args.wyjscie + " :" + args.mesa[0].split("!")[0][1:] + ": Done.")

def delfun(args):
	if len(args.mesa) > 4:
		args.botclass.functs.pop(args.mesa[4])

def czas(args):
	args.commqueue.put("PRIVMSG " + args.wyjscie + " :" + time.strftime("%d %B %Y %H:%M:%S"))

# Here they end

fts = {"seen": [seen, 0],
			"c": [calc, 0],
			"ogarnijnick": [ogarnick, 1],
			"orajpole": [quit, 1],
#			"setresplag" : [resplag, 1],
			"ident": [identify, 0],
			"whoami": [identified, 0],
			"whois": [whois, 0],
			"delfun": [delfun, 1],
			"czas": [czas, 0]}
