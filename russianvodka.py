#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import socket
import sqlite3
import threading
import Queue
import re
import time
import sys
import os
import signal
from bs4 import BeautifulSoup # soup is here!
import traceback
import ConfigParser
import json   # eh :D
import urllib # x2
import random
# -- here python importing ends and the hell starts. you shuould pray for people, who read that. --
import ircclass
import comargs

# reading config...
confa = ConfigParser.ConfigParser()
confa.readfp(open("config.cfg"))


# eh, that should be deleted, but it makes no sense ;D
ORIGNAME = confa.get('main', 'name')
NAME = ORIGNAME
HOST = confa.get('main' ,'host')
PORT = int(confa.get('main', 'port'))
CHAN = confa.get('main', 'chan')
LINK = confa.get('main', 'link')
PREFIX = confa.get('main', 'prefix')
# and here we define our lovely class <3
bot = ircclass.IRCClass(NAME, HOST, PORT, CHAN, PREFIX, LINK)

iflog = confa.get('log', 'enabled')
logchan = confa.get('log', 'logchan')

mods = confa.items('modules')

adms = confa.items('admins')

responses = confa.items("responses")

# DELETION OF REGEXPS BELOW IS HARMFUL. I'M VEEEEEEEEERY SERIOUS.
rl = re.compile("\[\[(.*?)\]\]") # USUNIĘCIE GROZI ŚMIERCIĄ - TRAGICZNĄ
rt = re.compile("\{\{(.*?)\}\}")

names = {}

czekpoint = 0
locked = 0

iw = {'fa': 'http://fa.uncyclopaedia.org/wiki/',
'zh-hk': 'http://zh.uncyclopedia.info/wiki/粵語:',
'no': 'http://ikkepedia.wikia.com/wiki/',
'pl': 'http://nonsensopedia.wikia.com/wiki/',
'ru': 'http://absurdopedia.wikia.com/wiki/',
'la': 'http://uncapaedia.wikia.com/wiki/',
'mg': 'http://hatsiklo.pedia.ws/wiki/',
'hu': 'http://unciklopedia.org/wiki/',
'ca': 'http://valenciclopedia.wikia.com/wiki/',
'de': 'http://de.uncyclopedia.wikia.com/wiki/',
'th': 'http://th.uncyclopedia.info/wiki/',
'sl': 'http://slovo.uncyc.org/wiki/',
'et': 'http://ebatsuklopeedia.org/wiki/',
'lt': 'http://juoko.pedija.org/wiki/',
'zh': 'http://uncyclopedia.wikiachina.cn/wiki/',
'tr': 'http://yansiklo.pedia.ws/wiki/',
'sv': 'http://psyklopedin.hehu.se/wiki/',
'da': 'http://da.uncyclopedia.wikia.com/wiki/',
'sr': 'http://sr.uncyc.org/wiki/',
'ar': 'http://beidipedia.wikia.com/wiki/',
'nl': 'http://nl.uncyclopedia.info/wiki/',
'zh-tw': 'http://zh.uncyclopedia.info/wiki/',
'ro': 'http://neciclopedie.org/wiki/',
'it': 'http://nonciclopedia.wikia.com/wiki/',
'pt': 'http://desciclo.pedia.ws/wiki/',
'vi': 'http://uncyclopedia.org/wiki/Babel:Vi/',
'ja': 'http://ja.uncyclopedia.info/wiki/',
'fi': 'http://fi.uncyclopedia.wikia.com/wiki/',
'es': 'http://inciclopedia.wikia.com/wiki/',
'he': 'http://eincyclopedia.wikia.com/wiki/',
'el': 'http://anegkyklopaideia.wikia.com/wiki/',
'ast': 'http://nunyepedia.wikia.com/wiki/',
'ko': 'http://ko.uncyclopedia.info/wiki/',
'cs': 'http://necyklopedie.wikia.com/wiki/',
'fr': 'http://desencyclopedie.wikia.com/wiki/',
'en': 'http://uncyclopedia.org/wiki/',
'wikt': 'http://pl.wiktionary.org/wiki/',
'commons': 'http://commons.wikimedia.org/wiki/',
'q': 'http://pl.wikiquote.org/wiki/',
's': 'http://pl.wikisource.org/wiki/',
'd': 'http://wikidata.org/wiki/',
'wikia': 'http://community.wikia.com/wiki/'}

# Here are antinoob and rest of shit stubs ;d

# loadmod is loading ONE module, loadmods is loading all modules ;D
def loadmod(bot, mod):
	try:
		toload = __import__(mod)
		reload(toload)
		print toload.fts
		bot.addtofunc(toload.fts)
		print bot.functs
	except:
		traceback.print_exc()

def loadmods(bot, modsy):
	for i in modsy:
		if i[1] == 'load':
			loadmod(bot, i[0])

def loadmodsforce(bot, modsy):
	for i in modsy:
		loadmod(bot, i)

def ladujmody(args):
	loadmodsforce(args.botclass, args.mesa[4:])
	args.commqueue.put("PRIVMSG " + args.wyjscie + " :" + args.mesa[0].split("!")[0][1:] + ": done.")

bot.addtofunc({"ladujmody": [ladujmody, 1]})

def addadms(bot, admsy):
	for i in admsy:
		print i
		if i[1] != "":
			bot.addadmin(i[0])

#def addresps(bot, respy):
#	for i in respy:
#		if i[1] != "":
#			bot.addresp(eval(i[0]), i[1])

def seensqlwrap(scc, kiedy, co, kto):
	try:
		scc.execute("SELECT * FROM seen WHERE kto=?", (kto,))
		if scc.fetchone():
			scc.execute(u"UPDATE seen SET kiedy=?, co=? WHERE kto=?", (kiedy, co, kto)) 
		else:
			scc.execute("INSERT INTO seen VALUES (?,?,?)", (kto, kiedy, co))
	except:
		traceback.print_exc()

#def parselink(mesa, scc):
#	usage = " ".join(mesa[3:])[1:].split(" ")
#	for i in usage:
#		if re.match("^http\:\/\/", i):
#			scc.execute("SELECT * FROM linki WHERE adr=?", (i,))
#			a = scc.fetchone()
#			if a:
#				return "Było: " + a[0] + " o " + a[1] + ": " + a[2]
#			else:
#				scc.execute("INSERT INTO linki VALUES (?,?,?)", (mesa[0].split("!")[0][1:], time.strftime("%Y-%m-%d %H:%M:%S"), i))

def parsetags(whatqueue, commqueue, botclass):
	while 1:
		while whatqueue.qsize() > 0:
			try:
				mesa = whatqueue.get().split("\r")[0].split(" ")
				a = rl.findall(" ".join(mesa[3:]))
				wyjscie = botclass.chan
				if mesa[2] == botclass.chan:
					wyjscie = botclass.chan
				if mesa[2] == botclass.nick:
					wyjscie = mesa[0].split("!")[0][1:]
				for i in a:
					if len(i) == 0 or i[0] == "#":
						continue
					if ((i.split(":")[0] == "użytkownik") or (i.split(":")[0] == "user") or (i.split(":")[0] == "User") or (i.split(":")[0] == "Użytkownik")) and not (re.search("/", i.split(":")[1])):
						try:
							link = "http://nonsensopedia.wikia.com/api.php?format=json&action=parse&prop=text&text={{Kreatywne2|" + i.split(":")[1] + "}}a{{Specjalna:Editcount/" + i.split(":")[1] + "}}"
							l = urllib.urlopen(link).read()
							j = json.loads(l)['parse']['text']['*']
							te = j.split("\n")[0][3:].split("a")
							k = reduce(lambda x,y: int(x)+int(y), te[0].split("+"))
							h = int("".join(te[1].split(u"\xa0")))
							commqueue.put("PRIVMSG " + wyjscie + " :" + botclass.link + i.split("|")[0].replace(" ", "_").replace("+", "%2B") + " edycji: " + str(h) + ", w tym " + str(k) + " kreatywnych")
						except:
							commqueue.put("PRIVMSG " + wyjscie + " :" + botclass.link + i.split("|")[0].replace(" ", "_").replace("+", "%2B"))
					elif i.split(":")[0] == "w":
						if i.split(":")[1] == "en" or i.split(":")[1] == "_en":
							commqueue.put("PRIVMSG " + wyjscie + " :http://en.wikipedia.org/wiki/" + ":".join(i.split(":")[2:]).replace(" ", "_").replace("+", "%2B"))
						elif len(i.split(":")) > 2 and len(i.split(":")[1]) == 2:
							commqueue.put("PRIVMSG " + wyjscie + " :http://" + i.split(":")[1] + ".wikipedia.org/wiki/" + ":".join(i.split(":")[2:]).replace(" ", "_").replace("+", "%2B"))
						else:
							commqueue.put("PRIVMSG " + wyjscie + " :http://pl.wikipedia.org/wiki/" + ":".join(i.split(":")[1:]).replace(" ", "_").replace("+", "%2B"))
					elif i.split(":")[0] in iw:
						commqueue.put("PRIVMSG " + wyjscie + " :" + iw[i.split(":")[0]] + ":".join(i.split(":")[1:]).replace(" ", "_").replace("+", "%2B"))
					else:
						commqueue.put("PRIVMSG " + wyjscie + " :" + botclass.link + i.split("|")[0].replace(" ", "_").replace("+", "%2B"))
				c = rt.findall(" ".join(mesa[3:]))
				for i in c:
					commqueue.put("PRIVMSG " + wyjscie + " :" + botclass.link + "Template:" + i.split("|")[0].replace(" ", "_").replace("+", "%2B"))
			except:
				traceback.print_exc()
		time.sleep(1)

def parse(whatqueue, commqueue, parserqueue, botclass):
	scbase = sqlite3.connect("scbase.db")
	scbase.text_factory = str
	scc = scbase.cursor()
	lastresptime = time.mktime(time.localtime())
	while 1:
		while (whatqueue.qsize() > 0):
			#b = unicode(whatqueue.get(), errors='replace')
			mesa = whatqueue.get().split("\r")[0].split(" ")
			if mesa[0] == "PING":
				commqueue.put("PONG " + mesa[1])
			if mesa[1] == "PONG":
				botclass.updatelag(time.mktime(time.localtime()))
			elif mesa[1] == "353":
				if mesa[4] == botclass.chan:
					for i in mesa[6:]:
						if i[0] == "+" or i[0] == "@": #freenode sensitive
							names[i[1:]] = i[0]
						else:
							names[i] = ""
				print names
				for i in names.keys():
					commqueue.put("WHOIS :" + i)
			elif mesa[1] == "330":
				botclass.addtoauthsyf({mesa[3]: mesa[4]})
			elif mesa[1] == "433":
				botclass.nickchange(botclass.nick + "_")
				commqueue.put("NICK :" + botclass.nick)
			elif mesa[1] == "376":
				# Yes, i know - plain text passwords are bad
				if confa.get('nickserv', 'use') == "yes":
					commqueue.put("PRIVMSG NickServ :GHOST " + botclass.orignick + " " + confa.get('nickserv', 'pass'))
					commqueue.put("NICK :" + botclass.orignick)
					commqueue.put("PRIVMSG NickServ :IDENTIFY " + confa.get('nickserv', 'name') + " " + confa.get('nickserv', 'pass'))
					botclass.nickchange(botclass.orignick)
				commqueue.put("JOIN " + botclass.chan)
				botclass.joined()
			if mesa[1] == "PRIVMSG":
				username = mesa[0].split("!")[0][1:]
				wyjscie = botclass.chan
				if mesa[2] == botclass.chan:
					wyjscie = botclass.chan
				if mesa[2] == botclass.nick:
					wyjscie = username
					if mesa[3] == ":Zyje":
						bot.czekpoint = time.time()
				if username == "Maryskaa": 
					continue
				scc.execute("SELECT kto FROM ignore WHERE kto=?", (username,))
				ignorecheck = scc.fetchone()
				if ignorecheck:
					continue
				scc.execute("SELECT kto FROM szukal WHERE kogo=?", (username,))
				szukany = scc.fetchall()
				if szukany:
					t = ''
					for i in szukany:
						if t == '':
							t = i[0]
						else:
							t = ", ".join([t, i[0]])
					wutaa = "NOTICE %s :Szukali Cię: %s" % (username, t)
					commqueue.put(wutaa)
					scc.execute("DELETE FROM szukal WHERE kogo=?", (username,))
				parserqueue.put(b) 				
				if mesa[3] != ':':
					if not (username in botclass.authsyf.keys()) and not (username in botclass.tested.keys()):
							commqueue.put("WHOIS :" + username)
							botclass.nicktested(username)
					if mesa[3][1] == botclass.prefix:
						if mesa[3][2:] in botclass.functs.keys():
							if botclass.functs[mesa[3][2:]][1] == 1:
								if (username in botclass.authsyf.keys()) and (botclass.authsyf[username].lower() in botclass.admins):
									try:
										botclass.functs[mesa[3][2:]][0](comargs.Comargs(mesa, commqueue, scc, botclass, wyjscie, names.keys()))
									except:
										commqueue.put("PRIVMSG " + wyjscie + " :" + traceback.format_exc().replace("\n", ";"))
								else:
									commqueue.put("PRIVMSG  " + wyjscie + " :" + mesa[0].split("!")[0][1:] + ": Nie masz uprawnień, aby używać tej funkcji.")
							else:
								try:
									botclass.functs[mesa[3][2:]][0](comargs.Comargs(mesa, commqueue, scc, botclass, wyjscie, names.keys()))
								except:
									commqueue.put("PRIVMSG " + wyjscie + " :" + traceback.format_exc().replace("\n", ";"))
						else:
							g = (mesa[3][2:],)
							scc.execute("SELECT co FROM comm WHERE kom=?", g)
							h = scc.fetchone()
							if h:
								commqueue.put("PRIVMSG " + wyjscie + " :" + h[0])
				seensqlwrap(scc, time.strftime("%d.%m.%Y %H:%M:%S", time.localtime()), unicode(" ".join(mesa[3:])[1:], errors="ignore"), username)
#				try:
#					byl = parselink(mesa, scc)
#					if byl:
#						commqueue.put("PRIVMSG " + wyjscie + " :" + byl)
#				except:
#					traceback.print_exc()
#				if len(mesa[3]) > 1:
#					if lastresptime+botclass.resplag < time.mktime(time.localtime()) and mesa[3][1] != botclass.prefix and random.uniform(1,10) >= 5:
#						for i in botclass.resps.keys():
#							try:
#								if re.search(i, " ".join(mesa[3:])):
#									commqueue.put("PRIVMSG " + wyjscie + " :" + botclass.resps[i])
#									lastresptime = time.mktime(time.localtime())
#									break
#							except:
#								traceback.print_exc()
			elif mesa[1] == "PART": 
				username = mesa[0].split("!")[0][1:]
				names.pop(username)
				seensqlwrap(scc, time.strftime("%d.%m.%Y %H:%M:%S", time.localtime()), "PART", username)
			elif mesa[1] == "JOIN":
				username = mesa[0].split("!")[0][1:]
				commqueue.put("WHOIS :" + mesa[0].split("!")[0][1:])
				names[username] = ""
				scc.execute("SELECT kto FROM szukal WHERE kogo=?", (username,))
				szukany = scc.fetchall()
				if szukany:
					t = ''
					for i in szukany:
						if t == '':
							t = i[0]
						else:
							t = ", ".join([t, i[0]])
					commqueue.put("NOTICE %s :Szukali Cię: %s" % (username, t))
					scc.execute("DELETE FROM szukal WHERE kogo=?", (username,))
				commqueue.put("WHOIS :" + mesa[0].split("!")[0][1:])
				seensqlwrap(scc, time.strftime("%d.%m.%Y %H:%M:%S", time.localtime()), "JOIN", username)
			elif mesa[1] == "QUIT":
				username = mesa[0].split("!")[0][1:]
				names.pop(username)
				seensqlwrap(scc, time.strftime("%d.%m.%Y %H:%M:%S", time.localtime()), "QUIT", username)
			if mesa[1] == "NICK":
				if botclass.onchannel == 1:
					username = mesa[0].split("!")[0][1:]
					names[mesa[2][1:]] = names[username]
					names.pop(username)
					drobnyca = "NICK %s" % (mesa[2])
					seensqlwrap(scc, time.strftime("%d.%m.%Y %H:%M:%S", time.localtime()), drobnyca, username)
					if mesa[0].split("!")[0][1:] in botclass.authsyf:
						botclass.authchange(mesa[0].split("!")[0][1:], mesa[2])
			if mesa[1] == "MODE":
				if mesa[3] == "+o":
					names[mesa[4].split("\r")[0]] = "@"
				if mesa[3] == "+v":
					names[mesa[4].split("\r")[0]] = "+"
		scbase.commit()
		time.sleep(1)

def logger(quepasa):
	# don't ask me why quepasa - it was ONLY another stupid name for another stupid variable for another stupid thread for another stupid functionality for another stupid bot!
	if(iflog == "1"):
		dd = time.strftime("%d")
		logfile = open("logs/" + logchan + "/" + time.strftime("%Y-%m-%d") + ".log", "a") # another stupid notation for another stupid logs.
		logfile.write("--- Log opened " + time.strftime("%a %b %d %H:%M:%S %Y") + "\n")
		licznik = 0
		while(1):
			while(quepasa.qsize() > 0):
				mesa = quepasa.get().split('\r')[0].split(" ")
				if mesa[0] == "PING" or mesa[1] == "PONG" or re.search("\d{3}", mesa[1]):
					continue
				uname = mesa[0].split("!")[0][1:]
				if len(mesa) > 2 and mesa[2] == logchan:
					if mesa[1] == "PRIVMSG":
						mesg = " ".join(mesa[3:]).split("\r")[0]
						uname = mesa[0].split("!")[0][1:]
						if mesg[1] == "\x01" and mesg[2:8] == "ACTION":
							logfile.write(time.strftime("%H:%M") + " * " + uname + " " + mesg[9:][:-1] +"\n")
						else:
							try:
								logfile.write(time.strftime("%H:%M") + " <" + names[uname] + uname + "> " + mesg[1:] +"\n")
							except:
								pass
					if mesa[1] == "PART":
						if len(mesa) > 3:
							a = time.strftime("%H:%M") + " -!- " + uname + " [" + mesa[0].split("!")[1] + "] has left " + logchan + " [" + " ".join(mesa[3:])[1:] + "]\n"
						else:
							a = time.strftime("%H:%M") + " -!- " + uname + " [" + mesa[0].split("!")[1] + "] has left " + logchan + "\n"
						logfile.write(a)
						logfile.flush()
					if mesa[1] == "KICK":
						if len(mesa) > 3:
							a = time.strftime("%H:%M") + " -!- " + mesa[3] + " was kicked from " + logchan + " by " + uname + " [" + " ".join(mesa[4:])[1:] + "]\n"
						else:
							a = time.strftime("%H:%M") + " -!- " + mesa[3] + " was kicked from " + logchan + " by " + uname + "\n"
						logfile.write(a)
						logfile.flush()
					if mesa[1] == "JOIN":
						a = time.strftime("%H:%M") + " -!- " + uname + " [" + mesa[0].split("!")[1] + "] has joined " + logchan + "\n"
						logfile.write(a)
						logfile.flush()
					if mesa[1] == "MODE":
						a = time.strftime("%H:%M") + " -!- mode/" + logchan + " [" + " ".join(mesa[3:]) + "] by " + uname + "\n"
						logfile.write(a)
						logfile.flush()
					if mesa[1] == "TOPIC":
						a = time.strftime("%H:%M") + " -!- " + uname + " changed topic of " + logchan + " to: " + " ".join(mesa[3:][1:]) + "\n"
						logfile.write(a)
						logfile.flush()
				if mesa[1] == "QUIT":
					if len(mesa) > 2:
						a = time.strftime("%H:%M") + " -!- " + uname + " [" + mesa[0].split("!")[1] + "] has quit " + logchan + " [" + " ".join(mesa[2:])[1:] + "]" + "\n"
					else:
						a = time.strftime("%H:%M") + " -!- " + uname + " [" + mesa[0].split("!")[1] + "] has quit " + logchan + "\n"
					logfile.write(a)
					logfile.flush()
				if mesa[1] == "NICK":
					logfile.write(time.strftime("%H:%M") + " -!- " + mesa[0].split("!")[0][1:] + " is now known as " + mesa[2][1:] + "\n")
					logfile.flush()

			logfile.flush()
			if(dd != time.strftime("%d")):
				logfile.write("--- Log closed " + time.strftime("%a %b %d %H:%M:%S %Y") + "\n")
				logfile = open("logs/" + logchan + "/" + time.strftime("%Y-%m-%d") + ".log", "a") # another stupid notation for another stupid logs.
				logfile.write("--- Log opened " + time.strftime("%a %b %d %H:%M:%S %Y") + "\n")
				dd = time.strftime("%d")
			if licznik == 1:
				time.sleep(1)
				licznik = 0
			else:
				licznik = 1


def linerecv(s):
	ret = ''
	while True:
		c = s.recv(1)
		if c == '\n' or c == '':
			break
		else:
			ret += c
	return ret

def commloop(s, que):
	while 1:
		try:
			b = linerecv(s)
			if b: que.put(b)
		except:
			break

if __name__ == "__main__":
	try:
		loadmods(bot, mods)
		addadms(bot, adms)
#		addresps(bot, responses)
		cq = Queue.Queue()  # ConnectionQueue (od serwera)
		pq = Queue.Queue()  # ParseQueue (od klienta)
		wq = Queue.Queue()  # WhatQueue (ConnectionQueue -> parser)
		npq = Queue.Queue() # backloop do parsera 
		lq = Queue.Queue()  # a to leci do loggera.
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		print "Trying to connect " + bot.host + ":" + str(bot.port)
		s.connect((bot.host, bot.port))
		print "Looks like connected..."
		threading.Thread(target=commloop, args=(s, cq)).start()
		s.send("USER gupobot 8 * :Bot do zbierania kur po wioskach!\n")
		s.send("NICK :" + bot.nick + "\n")
		threading.Thread(target=parse, args=(wq, pq, npq, bot)).start() # dwa wątki parsera, bo
		threading.Thread(target=parse, args=(wq, pq, npq, bot)).start() # jeden czasami zwiśnie na dłużej i co wtedy?
		threading.Thread(target=parsetags, args=(npq, pq, bot)).start() # ech, to parsowanie jest strasznie wolne (http related)
		threading.Thread(target=logger, args=(lq,)).start() # jak sama nazwa mówi - tu siedzi logger
		a=0
		while 1:
			while cq.qsize() > 0:
				b = cq.get()
				wq.put(b)
				lq.put(b)
				ce = b.split(" ")
				if len(ce) > 2 and ce[2].isdigit() and botclass.onchannel == True:
					continue
				print "%s %s" % (time.strftime("%d.%m.%Y %H:%M:%S", time.localtime()), b)
			while pq.qsize() > 0:
				tosend = pq.get()
				if len(tosend.split(" ")) > 1 and tosend.split(" ")[1] == "PRIVMSG":
					lq.put( ":" + bot.nick + "!gupobot@2001:41d0:a:2344:0:c0:ff:ee " + tosend) # hardcoded!
				print tosend
				s.send(tosend)
				s.send("\n")
			if a == 20:
				s.send("PRIVMSG " + bot.nick + " :Zyje\n")
				a = 0
			else:
				a = a+1
			time.sleep(1)
	except KeyboardInterrupt:
		s.send("QUIT :Bot mówi papa.\n")
		s.close()
		os.kill(os.getpid(), signal.SIGTERM)
	except:
		s.send("QUIT :Bot sie wysrał.\n")
		s.close()
		traceback.print_exc()
		os.kill(os.getpid(), signal.SIGKILL)	
