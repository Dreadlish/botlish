#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

class IRCClass:
	def __init__(self, nick='', host='', port=0, chan='', prefix='', link=''):
		self.nick=nick
		self.host=host
		self.port=port
		self.chan=chan
		self.prefix=prefix
		self.link=link
		self.orignick=nick
		self.onchannel=False
		self.functs={}
		self.authsyf={}
		self.admins=[]
#		self.resps={}
		self.tested={}
		self.lastpingtime=0
		self.lastlag=0
#		self.resplag=60
		self.czekpoint=0
	def nickchange(self, nick):
		self.nick=nick
	def joined(self):
		self.onchannel=1
	def addtofunc(self, what):
		if type(what) == dict:
			self.functs.update(what)
	def addadmin(self, admin):
		self.admins.append(admin)
	def deladmin(self, admin):
		self.admins.pop(admin)
	def addtoauthsyf(self, syf):
		self.authsyf.update(syf)
	def authchange(self, what, where):
		self.authsyf[where] = self.authsyf[what]
		self.authsyf.pop(what)
#	def addresp(self, reg, tekst):
#		if re.search("\/me", tekst):
#			outtext = tekst.replace("/me", "\x01ACTION") + "\x01"
#		else:
#			outtext = tekst
#		self.resps[reg] = outtext
#	def delresp(self, reg):
#		if self.resps.has_key(reg):
#			self.resps.pop(reg)
	def nicktested(self, nick):
		self.tested[nick] = 1
	def updatepingtime(self, pt):
		self.lastpingtime=pt
	def updatelag(self, pt):
		self.lastlag=pt-self.lastpingtime
#	def setresplag(self, rl):
#		self.resplag=rl
