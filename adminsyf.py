#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import time

def rawbase(args):
	if len(args.mesa) > 4:
		args.scc.execute(" ".join(args.mesa[4:]))

def addadm(args):
	if len(args.mesa) > 4:
		args.botclass.addadmin(args.mesa[4])

def pingalinga(args): #lag measurement
	args.botclass.updatepingtime(time.mktime(time.localtime()))
	commqueue.put("PING :troll")

def readlag(args):
	commqueue.put("PRIVMSG " + args.wyjscie + " :Ostatni zmierzony lag: " + str(args.botclass.lastlag) + "s")

def chgresplag(args):
	if len(args.mesa) > 4:
		args.botclass.setresplag(int(args.mesa[4]))
		commqueue.put("PRIVMSG " + args.wyjscie + " :Done.")

def rawprint(args):
	if len(args.mesa) > 4:
		commqueue.put(args.mesa[4:])

fts = {"rawbase": [rawbase, 1],
		"addadm": [addadm, 1],
		"measureping": [pingalinga, 0],
		"readlag": [readlag, 0],
		"chgresplag" : [chgresplag, 1],
		"rawprint": [rawprint, 1]}